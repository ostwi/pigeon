import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Broker from './views/Broker.vue';
import Sending from './views/Sending.vue';
import Receiving from './views/Receiving.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
  ],
});
