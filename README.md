# pigeon

Multiplatform application, which allows easy check  of the messages exchanged through a message broker. You can subscribe to different channels using a few protocols:

* AMQP 0.9.3
* AMPQ 1.0
* MQTT
* STOMP
